#!/bin/sh

case $1 in
  # Renew certificates, trust in certbot's algorithms
  renew) /usr/bin/certbot renew --quiet --agree-tos ;;
  bootstrap)
    for site in ${SUTTY} api.${SUTTY}; do
      test -d "/etc/letsencrypt/live/${site}" && exit 0

      # Get the certificate for the domain, the webserver will need
      # access to this directory
      /usr/bin/certbot certonly --email "certbot@${SUTTY}" \
                                --webroot \
                                --agree-tos \
                                --webroot-path /var/lib/letsencrypt \
                                -d "${site}"

      cd /etc/letsencrypt/live
      ln -s ${SUTTY} default
    done ;;
  # Generate certificates
  *)
    # Save headers here
    headers=/tmp/headers
    # Gets ETag from previous headers
    test -f "${headers}" \
      && etag="$(grep "^  Etag: " "${headers}" | cut -d : -f 2)"

    # Get site list from the API and transform to a list.  Save headers
    # for next run.  Use ETag to avoid running when nothing changed
    wget --user="${HTTP_BASIC_USER}" --password="${HTTP_BASIC_PASSWORD}" \
         --header="If-None-Match:${etag}" -qSO - \
          "https://api.${SUTTY}/v1/sites.json" \
          2>"${headers}" \
      | jq --raw-output .[] \
      | while read domain; do
          # Skip already existing domains
          test -d "/etc/letsencrypt/live/${domain}" && continue

          # Ignore non local domains
          nslookup "${domain}" 8.8.8.8 | grep -q "${SUTTY_ADDRESS}" || continue

          # Get the certificate for the domain, the webserver will need
          # access to this directory
          /usr/bin/certbot certonly --email "certbot@${SUTTY}" \
                                    --webroot \
                                    --agree-tos \
                                    --webroot-path /var/lib/letsencrypt \
                                    -d "${domain}"
        done
esac

# Fix permissions, users in group ssl have read access
find /etc/letsencrypt -type d | xargs -r chmod 2750
find /etc/letsencrypt -type f | xargs -r chmod 640
chgrp -R ssl /etc/letsencrypt

# Push certificates to nodes, we use SSH as a secure transport
# but this means we're synchronizing from container to host which is
# awkward.  A restricted rsync treats / as the remote location for the
# certificates.
for NODE in ${NODES}; do
  rsync -avHAX --delete-after /etc/letsencrypt/ ${NODE}:/
done

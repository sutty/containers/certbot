FROM sutty/monit:latest
MAINTAINER "f <f@sutty.nl>"

# Install requisites
RUN apk add --no-cache certbot jq wget

# Run certbot 
COPY ./monit.conf /etc/monit.d/certbot.conf

# Install certbot's script
COPY ./certbot.sh /usr/local/bin/certbot
RUN chmod +x /usr/local/bin/certbot

# Add ssl group
RUN addgroup -S -g 777 ssl

RUN apk add --no-cache openssh-client rsync
RUN install -dm 2750 -o root -g root /root/.ssh
COPY ./ssh_config /root/.ssh/config

# Access to certificates and challenges
VOLUME /etc/letsencrypt
VOLUME /var/lib/letsencrypt
